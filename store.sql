-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2022 at 07:56 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `store`
--

-- --------------------------------------------------------

--
-- Table structure for table `chart`
--

CREATE TABLE `chart` (
  `id` int(11) NOT NULL,
  `nama_barang` text NOT NULL,
  `jumlah` text NOT NULL,
  `harga` text NOT NULL,
  `no_pesanan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chart`
--

INSERT INTO `chart` (`id`, `nama_barang`, `jumlah`, `harga`, `no_pesanan`) VALUES
(1, 'HP Victus Gaming 16-E0089AX - Blue', '', '16499000', '1');

-- --------------------------------------------------------

--
-- Table structure for table `checkout`
--

CREATE TABLE `checkout` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `alamat` text NOT NULL,
  `no_telepon` text NOT NULL,
  `payment` text NOT NULL,
  `delivery` text NOT NULL,
  `total_harga` text NOT NULL,
  `no_pesanan` text NOT NULL,
  `id_pesanan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `checkout`
--

INSERT INTO `checkout` (`id`, `nama`, `alamat`, `no_telepon`, `payment`, `delivery`, `total_harga`, `no_pesanan`, `id_pesanan`) VALUES
(2, 'Irenea Putri Dewana', '23526436', '573456235', 'BRI', 'JNE', '16499000', '1', '1664843753');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `username` varchar(1000) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `id_pesanan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `username`, `password`, `id_pesanan`) VALUES
(1, 'joy', '$2y$10$TTxEi4KD5B921TeUoI43huppQ686sjJ0CqGdFGhnNkWJB2sHLymxi', '1664843753'),
(2, 'adhimbuck', '$2y$10$pqNe6v1ixE9zFq5E1yXsWeCvBHB9EzHqNaSgLoz8NwbezdPXxGoUC', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `gambar` text NOT NULL,
  `judul` text NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `gambar`, `judul`, `deskripsi`, `harga`) VALUES
(3, 'asus_tuf_fx517_black.jpg', 'Asus TUF FX517ZE-I7R5B7T-O - Off Black', '[i7 12650H-16GB-SSD 1TB-RTX3050Ti]', '20999000'),
(4, 'hp_gaming_victus_16_3.jpg', 'HP Victus Gaming 16-E0089AX - Blue', '[Ryzen 5 5600H-16GB-SSD 512GB-RTX3060]', '16499000'),
(8, 'legion_5_phantom_blue.jpg', 'Lenovo Legion 5-15ACH6H RPID - Phantom Blue', '[Ryzen 7 5800H-16GB-SSD 512GB-RTX3060]', '20499000'),
(9, 'katana_gf66_11.png', 'MSI Katana GF66-11UE-865ID - Black', ' [i5 11400H-16GB-SSD 512GB-RTX3060]', '16999000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chart`
--
ALTER TABLE `chart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checkout`
--
ALTER TABLE `checkout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chart`
--
ALTER TABLE `chart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `checkout`
--
ALTER TABLE `checkout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
