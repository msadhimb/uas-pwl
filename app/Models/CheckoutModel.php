<?php

namespace App\Models;

use CodeIgniter\Model;

class CheckoutModel extends Model
{
    protected $table = "checkout";
    protected $primaryKey = "id";
    protected $allowedFields = [
        "nama",
        "alamat",
        "no_telepon",
        "payment",
        "delivery",
        "total_harga",
        "no_pesanan",
        "id_pesanan"
    ];

    public function updateData($idUser, $idPesanan)
    {
        $builder = $this->db->table('member');
        $builder->set('id_pesanan', $idPesanan);
        $builder->where("id", $idUser);
        $builder->update();
    }
}
