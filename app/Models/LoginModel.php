<?php

namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model
{
    protected $table = 'member';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'username',
        'password',
        'id_pesanan'
    ];

    public function getDataUser($username)
    {

        $builder = $this->db->table('member');
        $builder->select('*');
        $builder->where("username", $username);
        $data = $builder->get()->getResult();
        return $data;
    }
}
