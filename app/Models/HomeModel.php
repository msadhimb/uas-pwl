<?php

namespace App\Models;

use CodeIgniter\Model;

class HomeModel extends Model
{
    protected $table = "product";
    protected $primaryKey = "id";
    protected $allowedFields = [
        "gambar", "deskripsi", "harga"
    ];

    public function  showData()
    {
        $builder = $this->db->table("product");
        $builder->select("*");
        $rs = $builder->get()->getResult();
        return $rs;
    }

    public function getDataIdPsn($idUser)
    {

        $builder = $this->db->table('member');
        $builder->select('id_pesanan');
        $builder->where("id", $idUser);
        $data = $builder->get()->getResult();
        foreach ($data as $row) {
            return $row->id_pesanan;
        }
    }
}
