<?php

namespace App\Models;

use CodeIgniter\Model;

class ChartModel extends Model
{
    protected $table = "chart";
    protected $primaryKey = "id";
    protected $allowedFields = [
        "nama_barang", "jumlah", "harga", "no_pesanan"
    ];

    public function showData($idUser)
    {
        $builderz = $this->db->table("chart");
        $builderz->select('*');
        $rs = $builderz->where('no_pesanan', $idUser);

        return $rs;
    }

    public function deleteData($idUser, $id)
    {
        $builder = $this->db->table("chart");
        $builder->select('*');
        $builder->where('id', $id);
        $builder->where('no_pesanan', $idUser);
        return $builder->delete();
    }
    public function showProduct($id)
    {
        $builder = $this->db->table("product");
        $builder->select('*');
        $builder->where('id', $id);
        $rs = $builder->get()->getResult();

        return $rs;
    }
}
