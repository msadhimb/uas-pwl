<?php

namespace App\Controllers;

use App\Models\HomeModel;
use App\Models\ChartModel;

class Home extends BaseController
{
    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->homeDtbs = new HomeModel();
    }
    public function index($id = null, $total_harga = null)
    {
        $rs = $this->homeDtbs->showData();
        $chekIdPsn = $this->homeDtbs->getDataIdPsn($id);
        $data = [
            "rs" => $rs,
            'session' => \Config\Services::session(),
            'idUser' => $id,
            'idPsn' => $chekIdPsn,
            'total_harga' => $total_harga
        ];
        return view('Home\home', $data);
    }

    public function chart($idUser, $id = null)
    {
        $chart = new ChartModel();
        $rs = $chart->showProduct($id);
        foreach ($rs as $databarang) {

            $data = [
                "nama_barang" => $databarang->judul,
                "jumlah" => '',
                "harga" => $databarang->harga,
                "no_pesanan" => $idUser,
            ];
            $chart->insert($data);
        }


        return redirect()->to('Chart/chart' . '/' . $idUser);
    }
}
