<?php

namespace App\Controllers;

use App\Models\ChartModel;

class Chart extends BaseController
{
    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->chart = new ChartModel();
    }

    public function chart($idUser)
    {

        $rs = $this->chart->showData($idUser);
        $data = [
            "rs" => $rs->get()->getResult(),
            'session' => \Config\Services::session(),
            'idUser' => $idUser
        ];

        return view('Chart\chart', $data);
    }

    public function delete($idUser, $id)
    {
        $this->chart->deleteData($idUser, $id);
        $rs = $this->chart->showData($idUser);
        $data = [
            "rs" => $rs->get()->getResult(),
            'session' => \Config\Services::session(),
            'idUser' => $idUser
        ];

        return view('Chart\chart', $data);
    }
}
