<?php

namespace App\Controllers;

use App\Models\HomeModel;
use App\Models\ChartModel;
use App\Models\CheckoutModel;


class Checkout extends BaseController
{
    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->checkoutDtbs = new CheckoutModel();
    }

    public function checkout($idUser)
    {
        $session = [
            "session" => \Config\Services::session(),
            "total" => $_POST['total'],
            'idUser' => $idUser
        ];
        return view('checkout\checkout', $session);
    }

    public function checkoutForm($idUser, $total)
    {
        $namaVar = $_POST['nama'];
        $alamatVar = $_POST['alamat'];
        $numVar = $_POST['num'];
        $paymentVar = $_POST['payment'];
        switch ($paymentVar) {
            case '1':
                $paymentVar = 'BRI';
                break;
            case '2':
                $paymentVar = 'BCA';
                break;
            case '3':
                $paymentVar = 'Mandiri';
                break;
        }
        $deliveryVar = $_POST['delivery'];
        switch ($deliveryVar) {
            case '1':
                $deliveryVar = 'JNE';
                break;
            case '2':
                $deliveryVar = 'JNT';
                break;
            case '3':
                $deliveryVar = 'SiCepat';
                break;
        }
        $nomorPesanan = $_POST['no_pesanan'];
        $idPesanan = crc32(rand());

        $data = [
            'nama' => $namaVar,
            'alamat' => $alamatVar,
            'num' => $numVar,
            'payment' => $paymentVar,
            'delivery' => $deliveryVar,
            'idUser' => $nomorPesanan,
            'idPesanan' => $idPesanan,
            'total_harga' => $total,
            'session' => \Config\Services::session(),
            'idUser' => $idUser

        ];

        $checkoutIns = [
            "nama" => $namaVar,
            "alamat" => $alamatVar,
            "no_telepon" => $numVar,
            "payment" => $paymentVar,
            "delivery" => $deliveryVar,
            "total_harga" => $total,
            'no_pesanan' => $nomorPesanan,
            'id_pesanan' => $idPesanan
        ];

        $this->checkoutDtbs->insert($checkoutIns);
        $this->checkoutDtbs->updateData($idUser, $idPesanan);

        return view('checkout\confirm', $data);
    }

    public function payment($idUser, $total)
    {
        if (isset($_POST['totalHarga'])) {
            $total = $_POST['totalHarga'];
        }

        $data = [
            'total_harga' => $total,
            'session' => \Config\Services::session(),
            'idUser' => $idUser
        ];

        return view('checkout\payment', $data);
    }
}
