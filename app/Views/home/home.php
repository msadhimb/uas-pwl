<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <style>
        hr {
            box-shadow: 0px 2px 3px black;
        }
    </style>
</head>

<body>
    <!-- Awal navbar -->
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg mt-2">
                        <div class="container">
                            <a class="navbar-brand" href="#">PWL Store</a>
                            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                                <div class="navbar-nav  ms-auto">
                                    <a class="nav-link" aria-current="page" href="#">Home</a>
                                    <?php if ($idPsn == '' && isset($_SESSION['sesUser'])) {
                                    ?>
                                        <a class="nav-link" aria-current="page" href="<?php echo base_url('home/chart') . '/' . $idUser ?>"><i class="fa-solid fa-cart-shopping"></i></a>
                                    <?php
                                    } else if (!isset($_SESSION['sesUser'])) {
                                        echo '';
                                    } else {
                                    ?>
                                        <a class="nav-link" aria-current="page" href="<?php echo base_url('checkout/payment') . '/' . $idUser . '/' . $total_harga ?>"><i class="fa-solid fa-money-bill-transfer"></i></i></a>
                                    <?php
                                    } ?>

                                    <?php if (!isset($_SESSION['sesUser'])) {
                                    ?>
                                        <a class="nav-link btn btn-primary text-white ms-3" aria-current="page" href="<?php echo base_url('login/login') ?>">Login</a>
                                    <?php
                                    } else {
                                    ?>
                                        <a class="nav-link btn btn-danger text-white ms-3" aria-current="page" href="<?php echo base_url('login/logout') ?>">Logout</a>
                                    <?php
                                    } ?>
                                </div>
                            </div>
                        </div>

                    </nav>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <!-- Akhir navbar -->

    <!-- Awal Header -->
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-6 ps-5 m-auto">
                    <h2>Welcome to PWL Store<?php if (!isset($_SESSION['sesUser'])) {
                                                echo '';
                                            } else {
                                                echo ', ' . $_SESSION['sesUser'];
                                            } ?></h2>

                    <p style=" font-size: 20px;" class="mt-5"> <strong> Best laptop gaming </strong> in Your gadget
                    </p>
                    <p style="font-size: 18px;">We provide a variety of high quality laptops <br> at a more affordable price than other e-commerce</p>
                </div>
                <div class="col-6">
                    <img src="../../../Product/gambarHeader.png" alt="" style="width:90%;">
                </div>
            </div>
            <hr>
        </div>
    </div>
    <!-- Akhir Header -->

    <!-- Konten (Our Product) -->
    <div class="container-fluid py-5">
        <div class="container">

            <div class="row">
                <div class="col-12 text-center">
                    <h2>Our Product</h2>
                    <p>Gaming Laptop Professional</p>
                </div>
            </div>
            <!-- row -->
            <div class="row d-flex justify-content-center">
                <?php
                foreach ($rs as $elements) {
                ?>
                    <div class="col-md-4 mt-3">
                        <div class="card mx-auto" style="width: 18rem;">
                            <img src='<?php echo '../../../Product/' . $elements->gambar ?>' class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $elements->judul ?></h5>
                                <p class="card-text"> <?php echo $elements->deskripsi ?> </p>
                                <p class="card-text text-end">Rp. <?php echo $elements->harga ?></p>
                                <div class="d-flex justify-content-end">
                                    <?php if (!isset($_SESSION['sesUser'])) {
                                    ?>
                                        <a class="btn btn-primary text-white" aria-current="page" href="<?php echo base_url('login/login') ?>">Order</a>
                                    <?php
                                    } else {
                                    ?>
                                        <a class="btn btn-primary text-white" aria-current="page" href="<?php echo base_url('home/chart') . '/' . $idUser . '/' . $elements->id ?>">Order</a>
                                    <?php
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
            <!-- akhir row -->

        </div>
    </div>
    <!-- Akhir Kontent 1 (Gallery) -->

    <!-- Awal Footer -->
    <div class="container-fluid">
        <div class="container">
            <hr>
            <div class="row p-5">
                <div class="col-12 text-center">Copyright © 2022 PWL Store. All Right Reserved.</div>
            </div>
        </div>
    </div>
    <!-- Akhir Footer -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ca3087f70b.js" crossorigin="anonymous"></script>
</body>

</html>