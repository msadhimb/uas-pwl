<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <style>
        body {
            overflow-x: hidden;
        }

        hr {
            box-shadow: 0px 2px 3px black;
        }
    </style>
</head>

<body>
    <!-- Awal navbar -->
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg mt-2">
                        <div class="container">
                            <a class="navbar-brand" href="#">PWL Store</a>
                            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                                <div class="navbar-nav  ms-auto">
                                    <a class="nav-link" aria-current="page" href="<?php echo base_url('home/index') . '/' . $idUser ?>">Home</a>
                                    <?php if (!isset($_SESSION['sesUser'])) {
                                    ?>
                                        <a class="nav-link btn btn-primary text-white ms-3" aria-current="page" href="<?php echo base_url('login/login') ?>">Login</a>
                                    <?php
                                    } else {
                                    ?>
                                        <a class="nav-link btn btn-danger text-white ms-3" aria-current="page" href="<?php echo base_url('login/logout') ?>">Logout</a>
                                    <?php
                                    } ?>
                                </div>
                            </div>
                        </div>

                    </nav>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="container" style="width: 50%;">
        <h1 class="text-center mt-5 mb-3">Chekcout Form</h1>
        <form method="post" action="<?php echo base_url('checkout/checkoutForm') . '/' . $idUser . '/' . $total ?>">
            <div class="mb-3">
                <label class="form-label">Name</label>
                <input type="text" class="form-control" name="nama">
            </div>
            <div class="mb-3">
                <label class="form-label">Address</label>
                <textarea type="text" class="form-control" name="alamat"></textarea>
            </div>
            <div class="mb-3">
                <label class="form-label">Phone Number</label>
                <input type="text" class="form-control" name="num">
            </div>
            <div class="mb-3">
                <label class="form-label">Payment</label>
                <select class="form-select" aria-label="Default select example" name="payment">
                    <option selected>Select Payment Method</option>
                    <option value="1">BRI</option>
                    <option value="2">BCA</option>
                    <option value="3">MANDIRI</option>
                </select>
            </div>
            <div class="mb-3">
                <label class="form-label">Delivery Service</label>
                <select class="form-select" aria-label="Default select example" name="delivery">
                    <option selected>Select Delivery Service</option>
                    <option value="1">JNE</option>
                    <option value="2">JNT</option>
                    <option value="3">SiCepat</option>
                </select>
            </div>
            <input type="hidden" value="<?php echo $idUser ?>" name="no_pesanan">
            <div class="mb-3 d-flex justify-content-center mt-5 mb-5">
                <button type="submit" class="btn btn-primary text-end">Submit</button>
            </div>
        </form>
    </div>

    <!-- Akhir navbar -->



    <!-- Awal Footer -->
    <div class="container-fluid">
        <div class="container">
            <hr>
            <div class="row p-5">
                <div class="col-12 text-center">Copyright © 2022 PWL Store. All Right Reserved.</div>
            </div>
        </div>
    </div>
    <!-- Akhir Footer -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>

</html>