<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <style>
        hr {
            box-shadow: 0px 2px 3px black;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-lg mt-2">
        <div class="container">
            <a class="navbar-brand" href="#">Salman Store</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav  ms-auto">
                    <a class="nav-link" aria-current="page" href="<?php echo base_url('home/index') . '/' . $idUser ?>">Home</a>
                    <?php if (!isset($_SESSION['sesUser'])) {
                    ?>
                        <a class="nav-link btn btn-primary text-white ms-3" aria-current="page" href="<?php echo base_url('login/login') ?>">Logo In</a>
                    <?php
                    } else {
                    ?>
                        <a class="nav-link btn btn-danger text-white ms-3" aria-current="page" href="<?php echo base_url('login/logout') ?>">Logout</a>
                    <?php
                    } ?>
                </div>
            </div>
        </div>
    </nav>

    <hr>
    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Barang</th>
                    <th scope="col">Jumlah</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($rs as $barang) { ?>
                    <tr>
                        <th scope="row"><?php echo $i ?></th>
                        <td><?php echo $barang->nama_barang ?></td>
                        <td>Otto</td>
                        <td>Rp. <?php echo $barang->harga ?></td>
                        <td><a href="<?php echo base_url('Chart/delete') . '/' . $idUser . '/' . $barang->id ?>" class="btn btn-danger"><i class="fa-solid fa-trash-can"></i></a></td>
                    </tr>
                <?php
                    $i++;
                } ?>
            </tbody>
        </table>
    </div>
    <div class="container d-flex justify-content-end">
        <?php
        $jum = 0;
        foreach ($rs as $sum) {
            $jum = $jum + $sum->harga;
        }
        ?>
        <h3>Total = Rp. <?php echo $jum ?></h3>
        <form action="<?php echo base_url('checkout/checkout') . '/' . $idUser ?>" method="post">
            <input type="hidden" value="<?php echo $jum ?>" name="total">
            <button class=" btn btn-primary text-end text-white ms-3" type="submit">Checkout</button>
        </form>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ca3087f70b.js" crossorigin="anonymous"></script>
</body>

</html>